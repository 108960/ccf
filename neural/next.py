from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import numpy

nlayer=3
activ='sigmoid'
optim='adam'
xnorm_flag=1
ynorm_flag=1
ntrain=5000
nlayer=4


traindataset='/home/limu/data/astro/time/lcurve2_1ms_pack%i.npz'

def load_data(trainids=1,validids=9,nblock=10,vblock=4):
    validdataset=traindataset%9
    
    npz=numpy.load(traindataset%trainids)
    xdata=numpy.concatenate([npz['set%i'%i] for i in range(1,nblock)])
    ydata=numpy.concatenate([npz['del%i'%i] for i in range(1,nblock)])

    xmin,xmax=numpy.min(xdata),numpy.max(xdata)
    if xnorm_flag == 1:
        xdata=(xdata-xmin)/(xmax-xmin)
    #xdata=xdata/numpy.max(xdata,axis=1)[:,numpy.newaxis]
    ymin,ymax=numpy.min(ydata),numpy.max(ydata)
    if ynorm_flag == 1:
        ydata=(ydata-ymin)/(ymax-ymin)

    xtrain=xdata[:ntrain]
    ytrain=ydata[:ntrain]

    npz_valid=numpy.load(validdataset)
    j=validids
    xdata=numpy.concatenate([npz_valid['set%i%i'%(j-1,i)] for i in range(vblock)])
    ydata=numpy.concatenate([npz_valid['del%i%i'%(j-1,i)] for i in range(vblock)])

    #xdata=npz_valid['lc']
    #ydata=npz_valid['delay']

    xmin,xmax=numpy.min(xdata),numpy.max(xdata)
    if xnorm_flag == 1:
        xdata=(xdata-xmin)/(xmax-xmin)
    #xdata=xdata/numpy.max(xdata,axis=1)[:,numpy.newaxis]
    ymin,ymax=numpy.min(ydata),numpy.max(ydata)
    if ynorm_flag == 1:
        ydata=(ydata-ymin)/(ymax-ymin)
    return xtrain,ytrain,xdata,ydata

xtrain,ytrain,xvalid,yvalid=None,None,None,None
#xtrain,ytrain,xvalid,yvalid=load_data(1,9)


def train_me(odir="bome/data/astro/",nepochs=100):
    orig_dim=xvalid.shape[1]
    model=Sequential()

    model.add(Dense(units=256, input_shape=(orig_dim,),activation=activ))
    model.add(Dense(units=64,activation=activ))
    model.add(Dense(units=16,activation=activ))
    model.add(Dense(units=1))

    model.compile(loss='mean_squared_error', optimizer=optim)
    #model.compile(loss='mean_squared_error', optimizer=optim)
  
    history=model.fit(xtrain,ytrain,epochs=nepochs,batch_size=64,validation_data=(xvalid,yvalid))
    print(history.history)
    if odir!=None:
        numpy.savez(odir+"history_%dlayer_%dtrain_%depochs_noynorm"%(nlayer,ntrain,nepochs),val_loss=history.history['val_loss'],loss=history.history['loss'])
    return model

def predict(model,xvalid,yvalid,odir=None):
    ypred=model.predict_on_batch(xvalid)
    ymin,ymax=yvalid.min(),yvalid.max()
    bb=numpy.ravel(ypred)
    if ynorm_flag == 1:
        bb=bb*(ymax-ymin)+ymin
        yvalid=yvalid*(ymax-ymin)+ymin
    diff=yvalid-bb
    #print(yvalid)
    #print(ypred)
    if odir!=None:
        numpy.savez(odir+"result_%dlayer_%dtrain_%depochs_noynorm"%(nlayer,ntrain,nepochs),yvalid=yvalid,ypred=bb)
    return diff

