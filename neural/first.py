import numpy
#import matplotlib.pyplot as plt

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

nlayer=3
activ='sigmoid'
optim='adam'
xnorm_flag=1
ynorm_flag=1
ntrain=5000
nepochs=100

traindataset='bome/data/astro/time/lcurve2_1ms_pack%i.npz'
validdataset=traindataset%9
#npz=[numpy.load(traindataset%i) for i in range(1,5)]
npz=numpy.load(traindataset%1)
#xdata=npz['lc']
#ydata=npz['delay']
xdata=numpy.concatenate([npz['set%i'%i] for i in range(1,10)])
ydata=numpy.concatenate([npz['del%i'%i] for i in range(1,10)])

xmin,xmax=numpy.min(xdata),numpy.max(xdata)
if xnorm_flag == 1:
	xdata=(xdata-xmin)/(xmax-xmin)
#xdata=xdata/numpy.max(xdata,axis=1)[:,numpy.newaxis]
ymin,ymax=numpy.min(ydata),numpy.max(ydata)
if ynorm_flag == 1:
	ydata=(ydata-ymin)/(ymax-ymin)

xtrain=xdata[:ntrain]
ytrain=ydata[:ntrain]

npz_valid=numpy.load(validdataset)
j=9
xdata=numpy.concatenate([npz_valid['set%i%i'%(j-1,i)] for i in range(4)])
ydata=numpy.concatenate([npz_valid['del%i%i'%(j-1,i)] for i in range(4)])
    
#xdata=npz_valid['lc']
#ydata=npz_valid['delay']

xmin,xmax=numpy.min(xdata),numpy.max(xdata)
if xnorm_flag == 1:
	xdata=(xdata-xmin)/(xmax-xmin)
#xdata=xdata/numpy.max(xdata,axis=1)[:,numpy.newaxis]
ymin,ymax=numpy.min(ydata),numpy.max(ydata)
if ynorm_flag == 1:
	ydata=(ydata-ymin)/(ymax-ymin)

xvalid=xdata

